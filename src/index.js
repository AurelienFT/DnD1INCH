import React from "react"
import ReactDOM from "react-dom"
import "./index.css"
import Home from "./App"
import Articles from "./scenes/Articles"
import Datas from "./scenes/Datas"
import Infographics from "./scenes/Infographics"
import Tutorials from "./scenes/Tutorials"
import Aggregator from "./scenes/Tutorials/Aggregation_Tutorial/aggregator"
import AggregatorArticle from "./scenes/Articles/Aggregation_Article/aggregator"
import Navbar from "./Navbar"
import { BrowserRouter as Router, Switch, Route } from "react-router-dom"

ReactDOM.render(
  <React.StrictMode>
    <Router>
      <Navbar />
      <Switch>
        <Route component={Home} exact path="/" />
        <Route component={Articles} exact path="/articles" />
        <Route component={Datas} exact path="/datas" />
        <Route component={Infographics} exact path="/infographics" />
        <Route component={Tutorials} exact path="/tutorials" />
        <Route component={Aggregator} exact path="/tutorial/aggregator" />
        <Route component={AggregatorArticle} exact path="/article/aggregator" />
      </Switch>
    </Router>
  </React.StrictMode>,
  document.getElementById("root"),
)
