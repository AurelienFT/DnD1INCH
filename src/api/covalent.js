const axios = require("axios").default

async function getTransferDataEth() {
  let data = await axios.get("https://dnd1inch.aurelienfoucault.fr/eth/data")
  return data.data
}

async function getTransferDataBsc() {
  let data = await axios.get("https://dnd1inch.aurelienfoucault.fr/bsc/data")
  return data.data
}

export { getTransferDataEth, getTransferDataBsc }
