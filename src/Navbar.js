import { useHistory } from "react-router-dom";

function Navbar() {
  let history = useHistory();

  function goToArticles() {
    history.push("/articles");
  }

  function goToTutorials() {
    history.push("/tutorials");
  }

  function goToInfographics() {
    history.push("/infographics");
  }

  function goToDatas() {
    history.push("/datas");
  }

  function goToHome() {
    history.push("/");
  }
  return (
    <header className="absolute top-0 left-0 right-0 z-20">
      <nav
        className="container mx-auto px-6 md:px-12 py-4"
        x-data="{ open: false }"
      >
        <div className="md:flex justify-between items-center">
          <div className="flex justify-between items-center">
            <button className="text-white" onClick={() => goToHome()}>
              <svg
                className="w-6 mr-2 fill-current"
                xmlns="http://www.w3.org/2000/svg"
                data-name="Capa 1"
                viewBox="0 0 16.16 12.57"
              >
                <defs />
                <path d="M14.02 4.77v7.8H9.33V8.8h-2.5v3.77H2.14v-7.8h11.88z" />
                <path d="M16.16 5.82H0L8.08 0l8.08 5.82z" />
              </svg>
            </button>

            <div className="md:hidden">
              <button className="text-white focus:outline-none">
                <svg
                  className="h-6 w-6"
                  viewBox="0 0 24 24"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    x-show="open === false"
                    d="M4 6H20M4 12H20M4 18H20"
                    stroke="currentColor"
                    strokeWidth="2"
                    strokeLinecap="round"
                    strokeLinejoin="round"
                  />
                  <path
                    x-show="open === true"
                    d="M6 18L18 6M6 6L18 18"
                    stroke="currentColor"
                    strokeWidth="2"
                    strokeLinecap="round"
                    strokeLinejoin="round"
                  />
                </svg>
              </button>
            </div>
          </div>

          <div className="hidden md:flex items-center">
            <button
              className="text-sm uppercase mx-3 text-white cursor-pointer hover:text-indigo-600"
              onClick={() => goToTutorials()}
            >
              Tutorials
            </button>
            <button
              className="text-sm uppercase mx-3 text-white cursor-pointer hover:text-indigo-600"
              onClick={() => goToInfographics()}
            >
              Infographics
            </button>
            <button
              className="text-sm uppercase mx-3 text-white cursor-pointer hover:text-indigo-600"
              onClick={() => goToDatas()}
            >
              Datas
            </button>
            <button
              className="text-sm uppercase mx-3 text-white cursor-pointer hover:text-indigo-600"
              onClick={() => goToArticles()}
            >
              Articles
            </button>
          </div>
        </div>

        {/*make this work*/}
        <div
          x-show="open === true"
          className="md:hidden flex flex-col w-full z-40 bg-indigo-600 rounded mt-4 py-2 overflow-hidden"
        >
          <button className="font-mitr text-sm uppercase text-gray-200 py-2 px-2 hover:bg-indigo-500">
            About us
          </button>
          <button className="font-mitr text-sm uppercase text-gray-200 py-2 px-2 hover:bg-indigo-500">
            Calendar
          </button>
          <button className="font-mitr text-sm uppercase text-gray-200 py-2 px-2 hover:bg-indigo-500">
            Contact us
          </button>
        </div>
      </nav>
    </header>
  );
}

export default Navbar;
