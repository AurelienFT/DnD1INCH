import { useEffect, useState } from "react"
import { getTransferDataEth, getTransferDataBsc } from "../../api/covalent"

function numberWithCommas(x) {
  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
}

function DataCard(props) {
  const LOGO_ETH =
    "https://upload.wikimedia.org/wikipedia/commons/thumb/0/05/Ethereum_logo_2014.svg/1200px-Ethereum_logo_2014.svg.png"
  const LOGO_BINANCE = "https://upload.wikimedia.org/wikipedia/commons/5/57/Binance_Logo.png"
  return (
    <div>
      <div className="container mx-auto px-4 md:px-12 relative z-10 flex items-center py-4">
        <div className="flex space-x-4 px-4 justify-around">
          <div className="bg-gradient-to-t from-black via-pink-900 to-pink-700  h-96 w-28 md:w-96 md:rounded-3xl rounded-full shadow-md relative flex flex-col items-center justify-between md:items-start py-5 md:p-5 transition-all duration-150">
            <img
              className="rounded-full w-16 h-16 shadow-sm absolute -top-8 transform md:scale-110 duration-700"
              src={props.type === "Binance" ? LOGO_BINANCE : LOGO_ETH}
              alt=""
            />

            <div className="transform -rotate-90 md:rotate-0 align-middle text-2xl font-semibold text-gray-200 text-center m-auto md:m-0 md:mt-8">
              {props.title}
            </div>
            {props.data ? (
              <p className="text-xl w-full font-bold text-gray-300 text-center font-light hidden md:block">
                {props.data}
              </p>
            ) : (
              <div className="loader text-center ">Loading...</div>
            )}
            <div className="flex w-full text-ownerSize text-gray-300 text-center justify-around" />
          </div>
        </div>
      </div>
    </div>
  )
}

function Datas() {
  const [apiValuesEth, setApiValuesEth] = useState(null)
  const [apiValuesBsc, setApiValuesBsc] = useState(null)
  useEffect(() => {
    async function fetchMyAPI() {
      getTransferDataEth().then(function (value) {
        setApiValuesEth(value)
      })
      getTransferDataBsc().then(function (value) {
        setApiValuesBsc(value)
      })
    }

    fetchMyAPI()
  }, [])
  return (
    <div className="bg-indigo-900 relative overflow-hidden">
      <div className="inset-0 bg-black opacity-25 absolute"></div>
      <div style={{ paddingLeft: "37%" }}>
        <div className="container mx-auto pl-32 relative z-10 flex items-center xl:py-20">
          <div className="grid grid-cols-3 gap-4">
            <div style={{ background: "orange" }}>
              <div class="bg-orange-lightest border-l-4 border-orange text-orange-dark p-4" role="alert">
                <p class="font-bold">Be Warned</p>
                <p>Fetching data can take a few minutes.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="container mx-auto pr-32 relative z-10 flex items-center py-24 xl:py-20">
        <div className="grid grid-cols-3 gap-4">
          <DataCard
            type="ETH"
            title={"Volume of swaps on the V3 on the ETH blockchain"}
            data={apiValuesEth ? `${numberWithCommas(apiValuesEth.total.toFixed(2))} $` : null}
          />
          <DataCard
            type="Binance"
            title={"Volume of swaps on the V3 on the BSC blockchain"}
            data={apiValuesBsc ? `${numberWithCommas(apiValuesBsc.total.toFixed(2))} $` : null}
          />
          <DataCard
            type="ETH"
            title={"Total users on ETH blockchain"}
            data={apiValuesEth ? `${apiValuesEth.usersTotal}` : null}
          />
          <DataCard
            type="Binance"
            title={"Total users on BSC blockchain"}
            data={apiValuesBsc ? `${apiValuesBsc.usersTotal}` : null}
          />
          <DataCard
            type="ETH"
            title={"Most swapped tokens on ETH blockchain"}
            data={
              apiValuesEth
                ? apiValuesEth.tokensSwapped.map(function (elem, idx) {
                    return <div>{`${idx + 1}- ${elem.name} ${numberWithCommas(elem.price.toFixed(2))}$`}</div>
                  })
                : null
            }
          />
          <DataCard
            type="ETH"
            title={"Most received tokens though swaps on ETH blockchain"}
            data={
              apiValuesEth
                ? apiValuesEth.tokensReceived.map(function (elem, idx) {
                    return <div>{`${idx + 1}- ${elem.name} ${numberWithCommas(elem.price.toFixed(2))}$`}</div>
                  })
                : null
            }
          />
          <DataCard
            type="Binance"
            title={"Most swapped tokens on BSC blockchain"}
            data={
              apiValuesBsc
                ? apiValuesBsc.tokensSwapped.map(function (elem, idx) {
                    return <div>{`${idx + 1}- ${elem.name} ${numberWithCommas(elem.price.toFixed(2))}$`}</div>
                  })
                : null
            }
          />
          <DataCard
            type="Binance"
            title={"Most received tokens though swaps on BSC blockchain"}
            data={
              apiValuesBsc
                ? apiValuesBsc.tokensReceived.map(function (elem, idx) {
                    return <div>{`${idx + 1}- ${elem.name} ${numberWithCommas(elem.price.toFixed(2))}$`}</div>
                  })
                : null
            }
          />
        </div>
      </div>
    </div>
  )
}

export default Datas
