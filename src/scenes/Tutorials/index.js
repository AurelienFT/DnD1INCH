import { useHistory } from "react-router-dom"

function TutorialCard(props) {
  const history = useHistory()
  return (
    <button onClick={() => history.push(props.linkToTutorial)}>
      <div className="container mx-auto px-6 md:px-12 relative z-10 flex items-center py-24 xl:py-10">
        <div className="flex space-x-4 px-4 justify-around">
          <div className="bg-gradient-to-t from-black via-pink-900 to-pink-700  h-72 w-28 md:w-96 md:rounded-3xl rounded-full shadow-md relative flex flex-col items-center justify-between md:items-start py-5 md:p-5 transition-all duration-150">
            <img
              className="rounded-full w-16 h-16 shadow-sm absolute -top-8 transform md:scale-110 duration-700"
              src="https://www.crypto-nation.io/cn-files/uploads/2021/01/1inch-Logo.png"
              alt=""
            />

            <div className="transform -rotate-90 md:rotate-0 align-middle text-2xl font-semibold text-gray-200 text-center m-auto md:m-0 md:mt-8">
              {props.title}
            </div>
            <ul className="text-lg text-gray-300 font-light hidden md:block">
              {props.skills.map((x) => {
                return <li>{x}</li>
              })}
            </ul>

            <div className="flex w-full justify-around">
              <button
                className=" rounded-full w-16 h-16 shadow-sm bg-pink-400 bg-opacity-40 backdrop-blur-lg"
                src="https://randomuser.me/api/portraits/women/17.jpg"
                alt=""
              >
                <img
                  className="p-4 w-16 h-16"
                  src="https://s2.coinmarketcap.com/static/img/coins/200x200/7411.png"
                  alt=""
                />
              </button>
              <button
                className="hidden md:block | rounded-full w-16 h-16 shadow-sm bg-yellow-400 bg-opacity-40 backdrop-blur-lg"
                src="https://randomuser.me/api/portraits/women/17.jpg"
                alt=""
              >
                <img
                  className="p-4 w-16 h-16"
                  src="https://s2.coinmarketcap.com/static/img/coins/200x200/7411.png"
                  alt=""
                />
              </button>
              <button
                className="hidden md:block | rounded-full w-16 h-16 shadow-sm bg-red-400 bg-opacity-40 backdrop-blur-lg"
                src="https://randomuser.me/api/portraits/women/17.jpg"
                alt=""
              >
                <img
                  className="p-4 w-16 h-16"
                  src="https://s2.coinmarketcap.com/static/img/coins/200x200/7411.png"
                  alt=""
                />
              </button>
            </div>
          </div>
        </div>
      </div>
    </button>
  )
}
function Tutorials() {
  return (
    <div className="bg-indigo-900 relative overflow-hidden">
      <div className="inset-0 bg-black opacity-25 absolute"></div>
      <div className="container mx-auto px-6 md:px-12 relative z-10 flex items-center py-24 xl:py-40">
        <div className="grid grid-cols-3 gap-4">
          <TutorialCard
            title={"Learn how to parse swaps"}
            skills={["Learn fetching", "Parsing parameters", "Analyze and aggregate"]}
            linkToTutorial={"/tutorial/aggregator"}
          />
        </div>
      </div>
    </div>
  )
}

export default Tutorials
