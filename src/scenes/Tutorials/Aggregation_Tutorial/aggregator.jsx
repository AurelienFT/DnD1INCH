export default function ArticleAggregator({ match }) {
  console.log(match.params.tutorialId)
  return (
    <div className="bg-indigo-900 relative overflow-hidden">
      <div className="inset-0 bg-black opacity-25 absolute"></div>
      <div className="container mx-auto mx-6 md:mx-12 relative z-10 items-center my-24 xl:my-40 bg-gray-100">

        <zero-md src="/aggregator/part1.md"></zero-md>
        <br></br>
        <iframe title="First Exercise" height="800px" width="100%" src="https://replit.com/@AurelienFT/First-Exercise-1INCH-Aggregation?lite=true" scrolling="no" frameborder="no" allowtransparency="true" allowfullscreen="true" sandbox="allow-forms allow-pointer-lock allow-popups allow-same-origin allow-scripts allow-modals"></iframe>
        <zero-md src="/aggregator/part2.md"></zero-md>
        <br></br>
        <iframe title="Second Exercise" height="800px" width="100%" src="https://replit.com/@AurelienFT/Second-Exercise-1INCH-Aggregation?lite=true" scrolling="no" frameborder="no" allowtransparency="true" allowfullscreen="true" sandbox="allow-forms allow-pointer-lock allow-popups allow-same-origin allow-scripts allow-modals"></iframe>
        <zero-md src="/aggregator/part3.md"></zero-md>

      </div>
    </div>
  )
}

// import Gist from "react-gist"
// function Tools() {
//   return (
//     <div className="bg-indigo-900 relative overflow-hidden">
//       <div className="inset-0 bg-black opacity-25 absolute"></div>
//       <div className="container mx-auto px-6 md:px-12 relative z-10 flex items-center py-24 xl:py-40">
//         <Gist id="8744304" />
//       </div>
//     </div>
//   )
// }

// export default Tools
