function Infographics() {
  return (
    <div className="bg-indigo-900 relative overflow-hidden">
      <div className="inset-0 bg-black opacity-25 absolute"></div>
      <div className="container mx-auto px-6 md:px-12 relative z-10 flex items-center py-24 xl:py-40">
        <img src="/infographie.jpg" alt="first infographie" />
      </div>
    </div>
  )
}

export default Infographics
