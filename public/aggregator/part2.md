Well payed you now have 10 swaps.

## 2. Analyze swap tokens

Let's take a look at a `Swapped` event here is an example of result :

```json
{
   "block_signed_at":"2021-05-05T02:51:39Z",
   "block_height":12371614,
   "tx_offset":141,
   "log_offset":288,
   "tx_hash":"0x5d982444214dd40d4919aeb07bf804bb89319d1a32d219be87bf5e24be602fce",
   "_raw_log_topics_bytes":null,
   "raw_log_topics":[
      "0xd6d4f5681c246c9f42c203e287975af1601f8df8035a9251f79aab5c8f09e2f8"
   ],
   "sender_contract_decimals":null,
   "sender_name":null,
   "sender_contract_ticker_symbol":null,
   "sender_address":"0x11111112542d85b3ef69ae05771c2dccff4faa26",
   "sender_address_label":"1inch V3",
   "sender_logo_url":null,
   "raw_log_data":"0x000000000000000000000000d998a128a1dc7b3d9d29234b3c0815bebc96d1ab0000000000000000000000002260fac5e5542a773aa44fbcfedf7c193bc2c599000000000000000000000000dbdb4d16eda451d0503b854cf79d55697f90c8df000000000000000000000000d998a128a1dc7b3d9d29234b3c0815bebc96d1ab0000000000000000000000000000000000000000000000000000000004b8fd0a0000000000000000000000000000000000000000000000019b57a39b049845d4",
   "decoded":{
      "name":"Swapped",
      "signature":"Swapped(address sender, address srcToken, address dstToken, address dstReceiver, uint256 spentAmount, uint256 returnAmount)",
      "params":[
         {
            "name":"sender",
            "type":"address",
            "indexed":false,
            "decoded":true,
            "value":"0xd998a128a1dc7b3d9d29234b3c0815bebc96d1ab"
         },
         {
            "name":"srcToken",
            "type":"address",
            "indexed":false,
            "decoded":true,
            "value":"0x2260fac5e5542a773aa44fbcfedf7c193bc2c599"
         },
         {
            "name":"dstToken",
            "type":"address",
            "indexed":false,
            "decoded":true,
            "value":"0xdbdb4d16eda451d0503b854cf79d55697f90c8df"
         },
         {
            "name":"dstReceiver",
            "type":"address",
            "indexed":false,
            "decoded":true,
            "value":"0xd998a128a1dc7b3d9d29234b3c0815bebc96d1ab"
         },
         {
            "name":"spentAmount",
            "type":"uint256",
            "indexed":false,
            "decoded":true,
            "value":"79232266"
         },
         {
            "name":"returnAmount",
            "type":"uint256",
            "indexed":false,
            "decoded":true,
            "value":"29640339358754555348"
         }
      ]
   }
}
```

You can see that the event have 6 parameters:
- `sender`: address of the sender
- `srcToken`: address of the smart contract of the source token
- `dstToken`: address of the smart contract of the destination token
- `dstReceiver`: address of the receiver
- `spentAmount`: amount of source token
- `returnAmount`: amount of destination token

In following REPL replace the three dots by the address (in lowercase) of the ChainLink contract and the right parameter to know how many swaps have ChainLink as destination token. 