# Learn how to fetch and understand events of aggregation on 1inch

### Prerequisites

- Have an REPL account

## Introduction

1inch has developed a protocol to make aggregation between protocols. In this tutorial, we will see how to get the data from the events and then analyse the parameters.

## 1. Get the swaps events

To get the swap data you can use the `Get Log events by contract address` endpoint with the 1inch V3 router address as parameter: `0x11111112542D85B3EF69AE05771c2dCCff4fAa26`.

Moreover we will use [Primer](https://www.covalenthq.com/docs/tools/primer-query) to match the `Swapped` events.

In the following REPL change `ADDRESS_HERE` by the contract address and `NAME_EVENT_HERE` by the name of the vent in the REPL just above.

