# Deep dive in V3 aggregation on 1INCH ETH/BSC and limitations

## 1. Data fetching

To analyse data, we need... data. So we found the contract address and start digging.

The topic that interest us is the topic `Swap` because it contains :
- sender address
- address source token
- address destination token
- amount source token
- amount destination token

To have more complete data we know to have more informations on the tokens such as
- contract decimals: to know if the number of tokens is a lot or just have a lot of decimals
- price: to know if the amount is reliable and to have a common currency to normalize data.

In order to get those datas I tried to look in Covalent but the endpoint for contract metadata is not available on Ethereum. So I looked on 1INCH API and there is a [token endpoint](https://api.1inch.exchange/swagger/ethereum/#/Tokens/TokensController_getTokens). 

Nice we now have the decimals on the blockchain Ethereum we just need prices and we are good on this blockchain.

For the prices I don't get all prices on Covalent or 1INCH the more useful API that I found was [ethplorer](https://github.com/EverexIO/Ethplorer/wiki/Ethplorer-API).

For the blockchain Binance, I get the token from the 1INCH API but for the prices I use the [pancake swap API](https://github.com/pancakeswap/pancake-info-api).

## 2. Statistiques

Now that we have all our data we started make our stats. We could use the parameter of the `Swap` events and the price to get :
- Volumes in USD (sum of all swap in USD)
- Users (all unique senders)
- Top tokens by blockchain (aggregation of events by their token source)

With the data that we fetched ([click here](add)) we can put some conclusions:
- There is 5 more volumes on Ethereum than BSC but more users on BSC so the swaps are bigger on Ethereum. This can be explained by the facts that (as we can see in stats) the most used tokens to swap is ETH on Ethereum and BUSD on BSC so maybe it's because the price of ETH is higher the transaction are getting more easily big.
- We can see that most the swaps are made with stable coins on BSC. Use stable coins is a pretty new usage in crypto so it can suggests that the users of BSC are more beginners in blockchain world.
- We can see that the TOP 5 tokens for swaps are used in 4/5 of the swaps. In contrary in the Ethereum it's only 1/3 of transaction. It correlate with the precedent conclusion that users are more beginners and stay on mainstream tokens. It can also be explained because their is less tokens in the BSC.

## 3. Problems/Limitations

We started to see the problems of working with a big dataset. At start we only had a front project and using the Covalent API as our API but the requests to fetch the data was very long. So we tried a first solution

- Having the old data in local on the frontweb ? Was a great idea because we didn't had to call again the API to have old data and we just make 1 call for the new data and everything was in browser but it turns out that the data was too heavy (3GO). So we couldn't use it in local because it make crash the reactjs process.

We tried to same less but it wasn't concluant either.

- Use a tampon backend server ? This is the solution we are using now. The script to fetch new data only run one time by day and the data is serve over a little http server. We are less synchronized but it's a better experience for the user and we transfer less data which is cool for planet.

## 4. Conclusions

We saw that even if the smart contract are the same in both blockchains the users use them differently and we can constat it in datas.

We learned a lot about working with big data and we lost a lot of time on it for the Hackathon.